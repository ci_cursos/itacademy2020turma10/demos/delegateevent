﻿using System;

namespace delegateevent
{
    class Program
    {
        static void Main(string[] args)
        {
            Microfone mic = new Microfone();
            mic.Acionado += ImprimeAlo;
            mic.Falar("Júlio Machado");
        }

        static void ImprimeAlo(string nome)
        {
            Console.WriteLine($"Alô {nome}!");
        }
    }
}
