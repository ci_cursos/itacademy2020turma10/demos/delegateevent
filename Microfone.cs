namespace delegateevent
{
    public class Microfone
    {
        public event FazAlgoDelegate Acionado;
        public void Falar(string texto)
        {
            if (Acionado != null)
            {
                Acionado(texto);
            }
        }
    }
}